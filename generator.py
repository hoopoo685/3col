from random import randint
import pickle

import networkx as nx
import matplotlib.pyplot as plt

from graphs import output_graph
from embed import embed
from tools import verify_all_edges, verify_all_edges_closed, deg
from smallk_tools import *
from myStat import count_vertex
# n_vertices = 500
# n_graph = 6

def min_generator(n_vertices = 500, n_graph = 6, save_mode = True):
    """
    Description
        given the lower bound for the number of nodes, it produces the graph and the color mapping of that graph
    """
    graph_idx = randint(0, n_graph)
    # graph_idx = 0
    print("Graph %d" % graph_idx)
    G, G_dict = output_graph(graph_idx)
    not_enough_vertices = True
    while not_enough_vertices:
        graph_idx = randint(0, n_graph)
        # graph_idx = 0
        print("Graph %d" % graph_idx)
        G2, G2_dict = output_graph(graph_idx)
        # print("G2_dict length: %d\n" % len(G2_dict), G2_dict)
        G3, G3_dict = embed(G.copy(), G2.copy(), G_dict.copy(), G2_dict.copy(), test = False)
        # following line is commented out for speedup
        # verify_all_edges_closed(E = G, color_dict = G_dict)
        edge = list(G_dict.keys())[0]
        if len(G_dict[edge].keys()) > n_vertices:
            not_enough_vertices = False
        if verify_all_edges_closed(E = G3, color_dict = G3_dict):
            G = G3.copy()
            G_dict = G3_dict.copy()
        else:
            print("G1\n", G, "\nG1_dict\n", G_dict)
            print("\nG2\n", G2, "\nG2_dict\n", G2_dict)
            print("\nG3\n", G3, "\nG3_dict\n", G3_dict)
            print("ERROR: NOT COLORABLE")
            break

    print("G_dict length: %d\n" % len(G_dict))
    edge = list(G_dict.keys())[0]
    n_node = len(G_dict[edge])
    print("G Length %d" % n_node)

    G_nx = nx.Graph()
    G_nx.add_edges_from(G)
    nx.draw(G_nx)
    plt.savefig("foo.png")
    if save_mode:
        pickle.dump((G, G_dict), open("4-critical - %d vertices.cPickle" % n_node, 'wb'))
    return G, G_dict

def only_graph_generator(n_vertices = 500, n_graph = 6):
    """
    Description
        given a
    """
    graph_idx = randint(0, n_graph)
    # graph_idx = 0
    print("Graph %d" % graph_idx)
    G = output_graph(graph_idx)[0]
    not_enough_vertices = True
    while not_enough_vertices:
        graph_idx = randint(0, n_graph)
        # graph_idx = 0
        # print("Graph %d" % graph_idx)
        G2 = output_graph(graph_idx)[0]
        # print("G2_dict length: %d\n" % len(G2_dict), G2_dict)
        G3 = embed(G.copy(), G2.copy(), only_graph = True)
        # following line is commented out for speedup
        # verify_all_edges_closed(E = G, color_dict = G_dict)
        # edge = list(G_dict.keys())[0]
        # print("n_vertex", count_vertex(G3))
        if count_vertex(G3) > n_vertices:
            not_enough_vertices = False
    return [edge for edge in G3]

if __name__ == '__main__':
    from smallk_tools import evaluate_smallk
    valid_smallk = True
    n_test = 10
    counter = 0
    while counter < n_test:
        counter += 1
        print("Counter", counter)
        G = min_generator(n_vertices = 20, n_graph = 6, save_mode=False)[0]
        valid_smallk = evaluate_smallk(G)
        if not valid_smallk:
            n_vertex = count_vertex(G)
            pickle.dump(G, open("4-critical - %d vertices.cPickle" % n_vertex, 'wb'))
            break
    if valid_smallk:
        print("Smallk is VALID")
    else:
        plot_smallk_coloring(G, 'G.col.res')
        print("Smallk is INVALID")
