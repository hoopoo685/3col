import os
import random
import time


def to_dimacs(G, filename, comment = "", header = ""):
    """
    Description
        given a list of edge tuples, if the filename is not present in the directory it converts the graph to DIMACS standard format and saves it to file and returns True, else it returns False.
    """
    import bisect
    unique_vertices = sorted(set(e[0] for e in G)|set(e[1] for e in G))
    missing_vertex = [i for i in range(1, max(unique_vertices)) if i not in unique_vertices]
    G_small = [(e[0]-bisect.bisect_right(missing_vertex, e[0]), e[1]-bisect.bisect_right(missing_vertex, e[1])) for e in G]
    unique_vertices = sorted(set(e[0] for e in G_small)|set(e[1] for e in G_small))
    vertex_map = {v:[] for v in unique_vertices}
    for edge in G_small:
        try:
            vertex_map[edge[0]].append(edge[1])
        except Exception as e:
            print(edge)
        # vertex_map[edge[1]].append(edge[0])
    f = open(filename, 'w+')
    f.write("c %s\n" % comment)
    f.write("p %s\n" % header)
    for vertex in unique_vertices:
        vertex_map[vertex].sort()
        for neighbor_idx in range(len(vertex_map[vertex])):
            f.write("e %d %d\n" % (vertex, vertex_map[vertex][neighbor_idx]))
    f.close()

def to_dimacs_double(G, filename, comment = "", header = ""):
    """
    Description
        given a list of edge tuples, if the filename is not present in the directory it converts the graph to DIMACS standard format and saves it to file and returns True, else it returns False.
    """
    unique_vertices = sorted(set(e[0] for e in G)|set(e[1] for e in G))
    vertex_map = {v:[] for v in unique_vertices}
    for edge in G:
        vertex_map[edge[0]].append(edge[1])
        vertex_map[edge[1]].append(edge[0])
    f = open(filename, 'w+')
    f.write("c %s\n" % comment)
    f.write("p %s\n" % header)
    for vertex in unique_vertices:
        for neighbor_idx in range(len(vertex_map[vertex])):
            f.write("e %d %d\n" % (vertex, vertex_map[vertex][neighbor_idx]))
    f.close()

def run_smallk(filename):
    """
    Description
        Given the filename it runs smallk to find possible instances of 3coloring. It then outputs the time it took smallk to run.
    """
    n_color = 3
    random_int = random.randint(1, 1024)

    # smallk_dir =  os.path.dirname(os.path.realpath("smallk"))
    os.system("echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ >> ../smallk/SmallkSrc/result.txt")
    os.system("echo >> ../smallk/SmallkSrc/result.txt")
    os.system("echo >> ../smallk/SmallkSrc/result.txt")
    os.system("echo Next Graph >> ../smallk/SmallkSrc/result.txt")
    time_init = time.time()
    os.system("../smallk/SmallkSrc/smallk %s %d %d >> ../smallk/SmallkSrc/result.txt" % (filename, random_int, n_color))
    time_diff = time.time() - time_init
    return time_diff

def extract_from_smallk(G, filename, return_type = 'list'):
    """
    Description
        given a graph G and a .col.res filename, it extract the smallk proposed coloring from the .col.res file and return the color map.
        return_type can be either a 'list' or a 'dictionary'
    """
    lines = open(filename, 'r').readlines()
    if len(lines) < 2:
        print("Smallk coloring file, %s, contrains less than two lines" % filename)
        return True
    color_list = []
    for line_idx, line in enumerate(lines):
        if (line_idx != 0 and "CLRS" in line):
            break
        elif(line_idx != 0):
            color_list.extend(int(i)%3 for i in line.split())
    if return_type == "dictionary":
        vertices = sorted(list(set([e[0] for e in G])|set([e[1] for e in G])))
        n_vertex = len(vertices)
        color_map = {vertices[i]:color_list[i] for i in range(n_vertex)}
        coloring = color_map
    elif return_type == 'list':
        coloring = color_list
    else:
        print("extract_from_smallk: invalid input for return_type")
    return coloring

def verify_color(G, filename):
    """
    Description
        given a graph and the filename of smallk coloring, it outputs if the coloring is valid for the graph
    """
    from tools import isValid
    lines = open(filename, 'r').readlines()
    if len(lines) < 2:
        print("Smallk coloring file, %s, contrains less than two lines" % filename)
        return True
    color_list = []
    for line_idx, line in enumerate(lines):
        if (line_idx != 0 and "CLRS" in line):
            break
        elif(line_idx != 0):
            color_list.extend(int(i)%3 for i in line.split())
    vertices = sorted(list(set([e[0] for e in G])|set([e[1] for e in G])))
    n_vertex = len(vertices)
    color_map = {vertices[i]:color_list[i] for i in range(n_vertex)}
    smallk_valid = isValid(G, color_map)
    if smallk_valid:
        print("Smallk coloring is valid")
    else:
        print("Smallk coloring is NOT valid!!!")

def evaluate_smallk(G, print_time = False):
    import time
    from myStat import count_vertex
    if os.path.isfile("G.col.res"):
        os.system("rm G.col.res")

    isValid = True
    n_edge = len(G)
    n_vertex = count_vertex(G)
    to_dimacs(G = G, filename = "G.col", comment = "nothing", header = "edge %d %d" % (n_vertex, n_edge))
    if print_time:
        print(run_smallk("G.col"))
    else:
        run_smallk("G.col")
    time.sleep(0.01)
    if os.path.isfile("G.col.res"):
        isValid = False
    return isValid

def plot_smallk_coloring(G, filename):
    """
    Description
        given a Graph and a .col.res filename, it plots the graph using the proposed coloring of smallk
    """
    import networkx as nx
    import matplotlib.pyplot as plt
    color_list = extract_from_smallk(G, filename)
    color_map = {0:'red', 1:'blue', 2:'green'}
    color_list = [color_map[i] for i in color_list]
    G1 = nx.Graph()
    G1.add_edges_from(G)
    nx.draw(G1,node_color = color_list,with_labels = True)
    plt.title("Smallk Error")
    plt.savefig("smallk error.png")



if __name__ == '__main__':
    import pickle
    G = pickle.load(open('4-critical - 509 vertices.cPickle', 'rb'))[0]
    print(evaluate_smallk(G, print_time=True))
    # plot_smallk_coloring(G, 'G.col.res')
