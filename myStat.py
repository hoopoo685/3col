def count_vertex(G):
    """
    Description
        given a list of edge tuples, it outputs the number vertices in the graph
    """
    n_vertices = len(set(e[0] for e in G)|set(e[1] for e in G))
    return n_vertices
