import random

def isValid(edge_list, color_dict):
    """
    Description
        Given a 3-coloring of graph, edge_list, it returns true if the coloring is valid.
    """
    valid = True
    for edge in edge_list:
        if color_dict[edge[0]] == color_dict[edge[1]]:
            valid = False
    return valid

def remove_edge(E, color_dict, edge):
    """
    Description
        Given an edge and a color_dict of a 4-critical graph E, it verifies if the 3-coloring stored in color_dict[edge] correspoding to the near-4-critical graph, E\{edge} is valid.
        i.e. returns true if valid
    """
    edge = (min(edge[0], edge[1]), max(edge[0], edge[1]))
    E.remove(edge)
    key = "%d_%d" % (edge[0], edge[1])
    return isValid(E, color_dict[key])

def verify_all_edges(E, color_dict):
    """
    Description
        Given a color_dict of a 4-0critical graph E, it performs remove_edge method on all edges of graph  E.
    """
    for edge in E:
        E_copy = E.copy()
        key = "%d_%d" % (edge[0], edge[1])
        print("%s validation: " % key, remove_edge(E_copy, color_dict, edge))

def verify_all_edges_closed(E, color_dict):
    """
    Description
        Given a color_dict of a 4-0critical graph E, it performs remove_edge method on all edges of graph  E.
    """
    colorable = True
    for edge in E:
        E_copy = E.copy()
        key = "%d_%d" % (edge[0], edge[1])
        if not remove_edge(E_copy, color_dict, edge):
            colorable = False
    if colorable:
        return True
    else:
        return False

def verify_all_edges(E, color_dict):
    """
    Description
        Given a color_dict of a 4-0critical graph E, it performs remove_edge method on all edges of graph  E.
    """
    for edge in E:
        E_copy = E.copy()
        key = "%d_%d" % (edge[0], edge[1])
        print("%s validation: " % key, remove_edge(E_copy, color_dict, edge))
def deg(i, G):
    """
    Description
        returns the degree of vertex i (belongs to graph G)
    """
    counter = len([e for e in G if i in e])
    return counter

def tup_xtoi(e, x, i):
    """
    Description
        Given an edge containing vertex x, it returns a new edge where x is replaced by i.
    """
    if x == e[0]:
        if i < e[1]:
            return (i, e[1])
        else:
            return (e[1], i)
    else:
        if e[0] < i:
            return (e[0], i)
        else:
            return (i, e[0])

def key_xtoi(key, x, i):
    """
    Description
        Given an edge containing vertex x, it returns a new edge where x is replaced by i.
    """
    if str(x) == key.split("_")[0]:
        if i < int(key.split("_")[1]):
            return "%d_%d" % (i, int(key.split("_")[1]))
        else:
            return "%d_%d" % (int(key.split("_")[1]), i)
    elif str(x) == key.split("_")[1]:
        if int(key.split("_")[0]) < i:
            return "%d_%d" % (int(key.split("_")[0]), i)
        else:
            return "%d_%d" % (i, int(key.split("_")[0]))
    else:
        print("key_xtoi has a bug")

def vertex_to_idx(v1, v2):
    """
    Description
        Given two vertices, v1 and v2, it return a sorted key, 'v1_v2'.
    """
    key = "%d_%d" % (min(v1, v2), max(v1, v2))
    return key

def new_graph(G1, G2, G2_dict):
    """
    Description
        increments the value of vertices of G2 graph and G2_dict according by the greatest vertex in G1.
    """
    G1_max = max(set([e[0] for e in G1]).union(set([e[1] for e in G1])))
    G = [(G1_max+e[0], G1_max+e[1]) for e in G2]
    G_dict = {}
    for edge_key in G2_dict:
        G_dict["%d_%d" % (int(edge_key.split("_")[0])+G1_max, int(edge_key.split("_")[1])+G1_max)] = {i+G1_max : G2_dict[edge_key][i] for i in G2_dict[edge_key].keys()}
    return G.copy(), G_dict.copy()

def tup_to_key(e):
    """
    Description
        Given an edge in form of a tuple, (v1, v2), it changes the format of the edge to a key of color dict, 'v1_v2'.
    """
    key = "%d_%d" % (e[0], e[1])
    return key

def random_vertex(G):
    """
    Description
        Given a graph G, it return a random edge, e, and the index of a vertex belonging to e such that degree of e[index] < 4
    """
    while True:
        e = random.sample(G, 1)[0]
        index = random.sample([0, 1], 1)[0]
        if deg(e[index], G) < 4:
            break
    return e, index
