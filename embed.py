import sys
from tools import *


def embed(G1, G2, G1_dict={}, G2_dict={}, test = False, only_graph = False):
	"""
	Description
		(i, j) is an edge from G1
	"""
	# increments all vertix labels of G2 by the maximum label in G1
	G2, G2_dict = new_graph(G1, G2, G2_dict)
	if test:
		ij, idx1, xy, idx2 = test
	else:
		# choose a random edge ij
		ij, idx1 = random_vertex(G1)
		# choose a random edge xy
		xy, idx2 = random_vertex(G2)
	# print("ij: ", ij, "idx1: ", idx1)
	# print("xy: ", xy, "idx2: ", idx2)
	G = update_graph(G1, ij, idx1, G2, xy, idx2)
	if only_graph:
		return G.copy()
	G_dict = update_dict(G1_dict, ij, idx1, G2_dict, xy, idx2)
	# verify_all_edges(E = G, color_dict = G_dict)
	# print("Final Color Dict")
	# for key in sorted(G_dict.keys()):
	# 	print(key, G_dict[key])
	return G.copy(), G_dict.copy()

def update_graph(G1, ij, idx1, G2, xy, idx2):
	"""
	Description
		Given two 4-critical graphs G1 and G2, it performs the embed operation and outputs another 4-critical graph G.
	"""
	# remove ij
	G1.remove(ij)
	# remove xy
	G2.remove(xy)
	# merge x with i
	contain_x = []
	for edge in G2:
		if xy[idx2] in edge:
			# print(edge)
			contain_x.append(edge)
	for edge in contain_x:
		G2.append(tup_xtoi(edge, xy[idx2], ij[idx1]))
		G2.remove(edge)
	# add jy
	for edge in G2:
		G1.append(edge)
	G1.append((ij[int(not idx1)], xy[int(not idx2)]))
	return G1


def update_dict(G1_dict, ij, idx1, G2_dict, xy, idx2):
	"""
	Description
		Given color mappings for 4-critical graphs G1 and G2, returns a color mapping for the resulting 4-critical graph G where G is a merging of G1 and G2.
	Input Types
		G1_dict:	a dictionary; maps edges of G1, e, onto another dictionary containing the 3-colorability of n4q graph G1\e
		ij:			a tuple
		G2_dict:	a dictionary; maps edges of G2, e, onto another dictionary containing the 3-colorability of n4q graph G1\e
	"""
	# find the difference between colors assigned to x and i mod 3 upon removal of the edges xy, and ij respectively.
	color_distance = (G1_dict[tup_to_key(ij)][ij[idx1]] - G2_dict[tup_to_key(xy)][xy[idx2]]) % 3
	# if color_distance:
	# 	print("color_distance %d" % color_distance)
	# increment mappings of G2 vertices by color_distance mod 3.
	for edge_key in G2_dict:
		G2_dict[edge_key] = {v:(G2_dict[edge_key][v]+color_distance)%3 for v in G2_dict[edge_key]}
	# in G2_dict keys, replace x with i.
	contain_x = []
	for edge_key in G2_dict:
		# in the inner dictionary, add i mapping
		G2_dict[edge_key][ij[idx1]] = G2_dict[edge_key][xy[idx2]]
		# in the inner dictionary, remove x mapping
		G2_dict[edge_key].pop(xy[idx2])
		if str(xy[idx2]) in edge_key:
			contain_x.append(edge_key)
	# in edge labels containing x, replace x with i
	for edge_key in contain_x:
		edge_key2 = key_xtoi(edge_key, xy[idx2], ij[idx1])
		G2_dict[edge_key2] = G2_dict[edge_key]
		G2_dict.pop(edge_key)
	key_iy = vertex_to_idx(ij[idx1], xy[int(not idx2)])
	key_ij = vertex_to_idx(ij[idx1], ij[int(not idx1)])
	key_jy = vertex_to_idx(ij[int(not idx1)], xy[int(not idx2)])
	dict_iy = G2_dict[key_iy].copy()
	# if dict_iy contain any mapping of i, it will overwrite the inner mappings of G1_dict
	dict_iy.pop(ij[idx1])
	dict_ij = G1_dict[key_ij].copy()
	# since the color distance has been dealt with, reassignment of inner mapping of i is redundant
	dict_ij.pop(ij[idx1])
	# add key, jy, to G_dict and map it to G1_key[ij] & G2_key[iy]
	G_dict = {}
	G_dict[key_jy] = {**G1_dict[key_ij], **G2_dict[key_iy]}
	# add color mappings of G2_dict[iy] to G1_dict
	for key in G1_dict:
		color_distance = (G1_dict[key][ij[idx1]] - G2_dict[key_iy][ij[idx1]]) % 3
		# print("Update: ", key, color_distance)
		temp_dict = {key2:(dict_iy[key2]+color_distance)%3 for key2 in dict_iy}
		G1_dict[key].update(temp_dict)
	# add color mappings of G1_dict[ij] to G2_dict
	# print("4> G1_dict[2_3]\n", G1_dict["2_3"])
	# print("1> iy ", key_iy, "\nG2_dict[iy]\n", G2_dict[key_iy])
	for key in G2_dict:
		color_distance = (G1_dict[key_ij][ij[idx1]] - G2_dict[key][ij[idx1]]) % 3
		temp_dict = {key2:(G2_dict[key][key2]+color_distance)%3 for key2 in G2_dict[key]}
		# G2_dict[key].update(temp_dict)
		G2_dict[key] = {**temp_dict, **dict_ij}
	# update content of G1_dict with content of G2_dict
	G_dict.update(G1_dict)
	G_dict.update(G2_dict)
	# remove keys ij and xy from G1_dict
	G_dict.pop(key_iy)
	G_dict.pop(key_ij)
	return G_dict
